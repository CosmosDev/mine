import configparser
import json
from getpass import getpass

import praw as praw


def generate_mine():
    username = input('Username: ')
    password = getpass('Password: ')

    config = configparser.ConfigParser()
    config.read('config.ini')

    client_id = config['config']['clientId']
    client_secret = config['config']['clientSecret']

    reddit = praw.Reddit(
        client_id=client_id,
        client_secret=client_secret,
        password=password,
        user_agent='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1',
        username=username
    )

    children = []

    for subreddit in reddit.user.subreddits(limit=None):
        subreddit_data = vars(subreddit)

        subreddit_data.pop('_reddit')
        subreddit_data.pop('_fetched')
        subreddit_data.pop('_path')

        children.append(
            {
                'kind': 't5',
                'data': subreddit_data
            }
        )

    listing = {
        'kind': 'Listing',
        'data': {
            'after': None,
            'dist': len(children),
            'modhash': None,
            'children': children,
            'before': None
        }
    }

    with open('mine.json', 'w') as outfile:
        json.dump(listing, outfile, indent=4)

    print('File successfully generated!')


if __name__ == '__main__':
    generate_mine()
