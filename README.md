# Mine for Reddit

Generate a `mine.json` file containing the full list of subscribed Subreddits for a user.

## Requirements

- Python 3.10+
- Reddit Account
- Client ID & Client Secret*

\* To register an app to Reddit and get the Client ID and Client secret, follow [this guide](https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example#first-steps).

## Installation

- Run the following command:

```commandline
pip install -r requirements.txt
```

- Replace `YOUR_CLIENT_ID` and `YOUR_CLIENT_SECRET` with your own in `config.ini`.

## Usage

Run:

```commandline
python mine.py
```
